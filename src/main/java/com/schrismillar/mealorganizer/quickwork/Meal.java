package com.schrismillar.mealorganizer.quickwork;

import static com.schrismillar.mealorganizer.util.CustomCollections.list;

import java.util.List;

public class Meal {
	private List<Recipe> items;
	
	public Meal() {
		items = list();
	}
	
	public void addRecipeToMeal(Recipe item) {
		items.add(item);
	}
}
