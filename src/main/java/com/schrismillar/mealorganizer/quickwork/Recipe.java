package com.schrismillar.mealorganizer.quickwork;

public class Recipe {
	private String ingredients;
	private String directions;
	
	private String name;
	
	public Recipe(String name, String ingredients, String directions) {
		this.name = name;
		this.ingredients = ingredients;
		this.directions = directions;
	}
}
