package com.schrismillar.mealorganizer.recipe.quantity;

public interface Quantity {
	public String getType();
	public double getValue();
}
