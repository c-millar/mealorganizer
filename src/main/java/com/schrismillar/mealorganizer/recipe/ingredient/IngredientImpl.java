package com.schrismillar.mealorganizer.recipe.ingredient;

public class IngredientImpl implements Ingredient {
	private String name;
	
	public IngredientImpl(String name) {
		this.name = name;
	}
	
	public String getName() {
		return name;
	}
}
