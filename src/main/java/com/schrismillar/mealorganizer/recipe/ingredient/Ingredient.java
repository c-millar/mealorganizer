package com.schrismillar.mealorganizer.recipe.ingredient;

public interface Ingredient {
	public String getName();
}
