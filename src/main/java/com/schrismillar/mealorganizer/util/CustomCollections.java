package com.schrismillar.mealorganizer.util;

import java.util.ArrayList;
import java.util.List;

public class CustomCollections {
	private CustomCollections() {
		//class should never be instantiated
	}
	
	public static <T> List<T> list() {
		return new ArrayList<T>();
	}
	
	public static <T> List<T> list(T... array) {
		List<T> list = new ArrayList<T>();
		
		for(int i = 0; i < array.length; i++) {
			list.add(array[i]);
		}
		
		return list;
	}
}
